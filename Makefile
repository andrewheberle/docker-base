S6_OVERLAY_VERSION ?= 3.1.6.2
BASE_TAG ?= latest
TAG ?= latest

# Seperate out tag as version numbers
MAJOR_VER = $(word 1,$(subst ., ,$(TAG)))
MINOR_VER = $(word 2,$(subst ., ,$(TAG)))
PATCH_VER = $(word 3,$(subst ., ,$(TAG)))
OTHER_VER = $(word 4,$(subst ., ,$(TAG)))

build-%: Dockerfile-%
ifndef IMAGE
	$(error IMAGE must be provided)
endif
	docker build -t "$(IMAGE):$(TAG)-$*$(BASE_TAG)" --build-arg BASE_TAG=$(BASE_TAG) --build-arg S6_OVERLAY_VERSION=$(S6_OVERLAY_VERSION) -f Dockerfile-$* .

release-%: Dockerfile-% build-%
ifndef IMAGE
	$(error IMAGE must be provided)
endif
	docker push "$(IMAGE):$(TAG)-$*$(BASE_TAG)"
	docker tag "$(IMAGE):$(TAG)-$*$(BASE_TAG)" "$(IMAGE):$*$(BASE_TAG)"
	docker push "$(IMAGE):$*$(BASE_TAG)"
	docker tag "$(IMAGE):$(TAG)-$*$(BASE_TAG)" "$(IMAGE):$*"
	docker push "$(IMAGE):$*"
ifneq ($(strip $(MAJOR_VER)),)
	docker tag "$(IMAGE):$(TAG)-$*$(BASE_TAG)" "$(IMAGE):$(MAJOR_VER)-$*$(BASE_TAG)"
	docker push "$(IMAGE):$(MAJOR_VER)-$*$(BASE_TAG)"
endif
ifneq ($(strip $(MINOR_VER)),)
	docker tag "$(IMAGE):$(TAG)-$*$(BASE_TAG)" "$(IMAGE):$(MAJOR_VER).$(MINOR_VER)-$*$(BASE_TAG)"
	docker push "$(IMAGE):$(MAJOR_VER).$(MINOR_VER)-$*$(BASE_TAG)"
endif
ifneq ($(strip $(PATCH_VER)),)
	docker tag "$(IMAGE):$(TAG)-$*$(BASE_TAG)" "$(IMAGE):$(MAJOR_VER).$(MINOR_VER).$(PATCH_VER)-$*$(BASE_TAG)"
	docker push "$(IMAGE):$(MAJOR_VER).$(MINOR_VER).$(PATCH_VER)-$*$(BASE_TAG)"
endif
ifneq ($(strip $(OTHER_VER)),)
	docker tag "$(IMAGE):$(TAG)-$*$(BASE_TAG)" "$(IMAGE):$(MAJOR_VER).$(MINOR_VER).$(PATCH_VER).$(OTHER_VER)-$*$(BASE_TAG)"
	docker push "$(IMAGE):$(MAJOR_VER).$(MINOR_VER).$(PATCH_VER).$(OTHER_VER)-$*$(BASE_TAG)"
endif
